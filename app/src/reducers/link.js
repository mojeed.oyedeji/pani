import {FETCH_LINK, FETCH_LINKS} from  "../constants/link";


const initialState = {
  link:{
  },
  link:[]
};

function reducer(state = initialState, action) {
  switch (action.type){
    case FETCH_LINK:
      return Object.assign({}, state, {
         link: action.payload
       });
    case FETCH_LINKS:
      return Object.assign({}, state, {
          links: action.payload
      });
  }
  return state;
}

export default reducer;
