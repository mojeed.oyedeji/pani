import {defaultHandler, getHandler} from '../requests/global.js';
import {unexpectedError, wait, notify} from './index';
import {FETCH_LINK, FETCH_LINKS} from '../constants/link';



export function shorten(body){
  const url = '/shorten';
  return async dispatch => {
    dispatch(wait(true))
      const [res, status] = await defaultHandler(url, body, 'POST');
      console.log(res)
      if(status == "ok"){
        if(res.shorten_url){
          dispatch(fetchLink(res));
        }else{

        }
      }else{
        dispatch(unexpectedError())
      }
      dispatch(wait(false))
    };
}

export function getLinks(){
  const url = '/links';
  return async dispatch => {
    dispatch(wait(true))
      const [res, status] = await getHandler(url, {}, 'GET');
      if(status == "ok"){
        if(res){
          dispatch(fetchLinks(res));
        }else{

        }
      }else{
        dispatch(unexpectedError())
        console.log("error")
      }
      dispatch(wait(false))
    };
}


export function fetchLinks(payload){
  return { type: FETCH_LINKS, payload }
}

export function fetchLink(payload){
  return { type: FETCH_LINK, payload }
}




