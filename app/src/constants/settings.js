export const uri =
  {
    test: "http://localhost:5000",
    dev: "https://api.chetaa.me",
    live: "https://snt-api.thelastenvoy.com",
    mode: "test",
  }
