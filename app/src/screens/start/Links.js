import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import ListItem from '@mui/material/ListItem';
import Pagination from '@mui/material/Pagination';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import CopyIcon from '@mui/icons-material/CopyAllRounded';
import Divider from '@mui/material/Divider';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import {task1} from '../../images/task1.jpeg'

import {getLinks } from '../../actions/link';
import { notify } from '../../actions';
const classes = {
    root:{
        marginTop:10
    }
}

const styles = {
    media: {
      height: '100%',
      paddingTop: '75%', // 16:9,
      marginTop:'30'
    }
};
export default function Main(){
    const history = useHistory();
    const dispatch = useDispatch();
    const [form, setForm] = useState({url: ""});    
    const links = useSelector(state => state.links.links);

    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [startIndex, setStartIndex] = useState(0);
    const [endIndex, setEndIndex] =  useState(itemsPerPage - 1);
    const [view, setView] = React.useState(1);
    
    const [data, setData] = useState(links ? links.slice().sort((a, b) =>
                            new Date(a.created_date) < new Date(b.created_date) ? 1 : -1): []);



    useEffect(() => {
       dispatch(getLinks({}))
    }, [])

    useEffect(() => {
        setData(links ? links : [])
    }, [links])

    const handlePageChange  = (event, value) => {
        setView(value);
        setStartIndex(itemsPerPage*(value -1))
        setEndIndex(value*itemsPerPage - 1);
      }


    return(
      <div className={classes.root}>
      <Grid justifyContent="center" style={{height:"100%"}} alignItems="center" container direction="row">
        <Grid item xs={12} md={8} style={{marginTop:0, padding:10}}>
            <Card  elevation={0} style={{marginTop:'10%',backgroundColor: "#f5f5f5", borderRadius:20}} color="primary">
            <CardContent style={{padding:0}}>
            
            <ListItem divider>
                      <Grid container direction="row" justifyContent="space-between">
                        <Grid item md={2}>
                            <Typography color="textSecondary" variant="body1">DATE-TIME </Typography>
                        </Grid>
                        <Grid item md={3}>
                            <Typography color="textSecondary" variant="body1">URL </Typography>
                        </Grid>
                        <Grid item md={2}>
                            <Typography color="textSecondary" variant="body1">SLUG </Typography>
                        </Grid>
                        <Grid item md={3}>
                            <Typography color="textSecondary" variant="body1">SHORTEN URL </Typography>
                        </Grid>
                      </Grid>
                </ListItem>
            {data.map((item, index) => (
                 index >= startIndex && index <= endIndex && 
                <ListItem divider>
                      <Grid container direction="row" justifyContent="space-between">
                        <Grid item md={2}>
                            <Typography variant="body1">{item.created_date.substr(0,16)} </Typography>
                        </Grid>
                        <Grid item md={3}>
                            <Typography variant="body1" noWrap>{item.url} </Typography>
                        </Grid>
                        <Grid item md={2}>
                            <Typography variant="body1">{item.slug} </Typography>
                        </Grid>
                        <Grid item md={3}>
                            <Typography variant="body1">{item.shorten_url} </Typography>
                        </Grid>
                      </Grid>
                </ListItem>
            ))}
          
            
            


            
          
            

     
           
         
            
        </CardContent>
        <CardActions style={{justifyContent:"center"}}>
                <Pagination page={view} 
                              onChange={handlePageChange}
                              count={Math.ceil(data.length/itemsPerPage)}  />
                </CardActions>
        </Card>

        <div style={{marginTop:20}}>
            <Button startIcon={<ChevronLeftIcon />} onClick={() => history.push("/")}> <Typography>Back</Typography> </Button>
        </div>

      
    
        </Grid>
      </Grid>
           
        </div>
    )
}