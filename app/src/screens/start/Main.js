import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import CopyIcon from '@mui/icons-material/CopyAllRounded';
import Divider from '@mui/material/Divider';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import {task1} from '../../images/task1.jpeg'

import { shorten, fetchLink } from '../../actions/link';
import { notify } from '../../actions';
const classes = {
    root:{
        marginTop:10
    }
}

const styles = {
    media: {
      height: '100%',
      paddingTop: '75%', // 16:9,
      marginTop:'30'
    }
};
export default function Main(){
    const history = useHistory();
    const dispatch = useDispatch();
    const [form, setForm] = useState({url: ""});
    const link = useSelector(state => state.links.link);
    const [shortened, setShortened] = useState("");



    useEffect(() => {
        dispatch(fetchLink({}))
    }, [])

    useEffect(() => {
        setShortened(link.shorten_url);
    }, [link])

    function handleChange(event){
        const {name, value} = event.target
        setForm(form => ({...form, [name]: value}));
    }

    function handleClick(){
        if(form.url != ""){
            dispatch(fetchLink({}))
            setShortened("")
            dispatch(shorten(form))
        }else{
            dispatch(notify({message:"Please provide a link", status:"error"}))
        }

        
    }

    function handleCopied(){
        dispatch(notify({message:"copied", status:"success"}))
    }


    return(
      <div className={classes.root}>
      <Grid justifyContent="center" style={{height:"100%"}} alignItems="center" container direction="row">
        <Grid item xs={12} md={4} style={{marginTop:0, padding:10}}>
            <Card  elevation={0} style={{marginTop:'45%',backgroundColor: "#f5f5f5", borderRadius:20}} color="primary">
            <CardContent style={{padding:0}}>
            
            <div style={{marginTop:20, padding:10}}>
            <TextField fullWidth 
                      name="url" 
                      label="Original Link" 
                      variant="standard"
                      onChange={handleChange} />
            </div>
            
            <div style={{marginTop:20, marginBottom:20, padding:10}}>
            <Grid container direction="row">
                <Grid item md={11}>
                <TextField disabled 
                           fullWidth 
                           name="link" 
                           value={shortened}
                           label="Shortened Link" 
                           variant="standard" />
                </Grid>
                <Grid item md={1}>
                <CopyToClipboard text={shortened} onCopy={() => handleCopied()}>
                <IconButton color="primary" aria-label="delete">
                    <CopyIcon fontSize="large" />
                </IconButton>
                </CopyToClipboard>
                </Grid>
            </Grid>
            
            
            </div>

            
            
            


            
          
            

     
           
         
            
        </CardContent>
        <CardActions style={{padding:0}}>
       
                <Button onClick={handleClick} style={{borderRadius:0}} size="large" fullWidth variant="contained">
                    <Typography variant="h6"><b>Shorten</b></Typography>
                </Button>
           
        </CardActions>
        </Card>

        <div style={{marginTop:20}}>
            <Button onClick={() => history.push("/all")}> <Typography>All Links</Typography> </Button>
        </div>

      
    
        </Grid>
      </Grid>
           
        </div>
    )
}