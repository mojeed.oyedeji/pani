# SNT Advanced Software Developer Assessment 

The front end development was implemented with React.js. 

## Frameworks & Packages

Material UI (v5) was used for implementing most of the front end components.

Redux was used for state management.

React Router for navigation.

Google Charts for data displays.

For more see package.json file


## How to run the project

Download or clone this repository

Open terminal in the directory containing the app

Type `npm install` to install all project dependencies

Browse to constants folder and open settings.js 

Change between live or test mode by changing the "mode" property to "live" or "test"

Changing to "live" means the api requests will be served by https://snt-api.thelastenvoy.com

Changing to "test" means the api requests will be served locally on your choice local IP, the default used on the machine used to develop this project is http://127.0.0.20. If you wish to run locally you need to edit the "test" property accordingly.


Now run `npm start` to run the project locally.

Alternatively you can view this project at https://snt-app.thelastenvoy.com, the server is a bit slugglish due to limited resources but be patient.






