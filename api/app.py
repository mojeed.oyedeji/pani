from gettext import find
import json
from operator import truediv
from flask_cors import CORS
from venv import create
from flask import Flask, jsonify, request, redirect
from datetime import datetime
import string
import random


app = Flask(__name__)
CORS(app)

def load():
    with open('links.json') as json_file:
        try:
            linked = json.load(json_file)
        except json.JSONDecodeError:
            linked = []
            pass
    return linked


def find_in_links(slug):
    linked = load()
    if(len(linked) > 0):
        for item in linked:
            if item['slug'] == slug:
                return True
        else:
            return False
    else:
        return False


@app.route("/<slug>", methods=['GET'])
def hello_world(slug):
    linked = load();
    if(len(linked) > 0):
        for item in linked:
            if item['slug'] == slug:
                return redirect(item["url"])
    return "<p> Link not found </p>"



@app.route("/shorten", methods=['POST'])

def shorten():
    data = request.json
    chars = string.ascii_lowercase
    size = 6

    linked = load();

    slug = ''.join(random.choice(chars) for _ in range(size))
    
    while find_in_links(slug) == True:
        slug = ''.join(random.choice(chars) for _ in range(size))
    
    url = data.get("url");
    created_date = datetime.now();
    shorten_url = 'http://localhost:5000/'+ slug
    new_entry = {'url': url, 'created_date': str(created_date), 'slug': slug, 'shorten_url': shorten_url}
    new_entry = new_entry.copy()
    linked.append(new_entry)

    with open('links.json', 'w') as f:
        json.dump(linked, f)

    return new_entry


@app.route("/links", methods=['GET'])
def get_links():
    linked = load();
    return jsonify(linked)