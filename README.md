# ABOUT
A simple link shortnening microservice served from localhost.

# ASSUMPTIONS
Although the application runs from localhost, it is assumed that a short domain (pni.ly) is available to host the service. 

# DESIGN

A file json (links.json)  was used for storage. The attributes for each object in the json array include:

    1. url
    2. created_date
    3. slug 
    4. shorten_url

A simple api service was provided with the following endpoints:

    1. /<slug> -> Accepts slug as parameter and redirects to the original link.
    2. /links -> returns all the entries in the json file
    3. /shorten -> Accepts original link and returns an object containing the shortened url.


The slug consists of five-letter lowercase alphabets. This allows for approximately 7,893,600 links to be shortened. 

The shorten function will generate a slug (five-letter word), search for this word in the database (json file) and regenerate the slug 
if it already exists. If not, an object containing all the attributes specified above is added to the json array.


# TOOLS
React.JS, Redux, Material UI, Python, Flask.

# LAUNCH

    1. Run docker-compose build
    2. Run docker-compose up -d

The app runs on localhost:4001, and the api (server) runs on localhost:5000


